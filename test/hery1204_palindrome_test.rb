require "test_helper"
require "./lib/hery1204_palindrome.rb"

class Hery1204PalindromeTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Hery1204Palindrome::VERSION
  end

  def test_it_does_something_useful
    assert true
  end

  def test_non_palindrome
  	refute !"apple".palindrome?
  end

  def test_literal_palindrome
  	assert "racecar".palindrome?
  end

  def test_mixed_case_palindrome
    assert "Racecar".palindrome?
  end

  def test_palindrome_with_punctuation
    assert "raceCar?".palindrome?
  end
end
