require "hery1204_palindrome/version"

class String

	# Returns true for a palindrome, false otherwise.
	def palindrome?
		processed_content == processed_content.reverse
	end

	private

		#Returns content for palindrome Setting.
		def processed_content
			self.downcase
		end
end
